import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Grid from 'material-ui/Grid';
import { MuiThemeProvider } from 'material-ui/styles';

import Theme from '../themes/primary';
import ButtonTemplate from '../components/button/button.template.js';

storiesOf('Button', module)
  .add('with text', () => (
    <button onClick={action('clicked')}>Hello Button</button>
  ))

  .add('with some emoji', () => (
    <button onClick={action('clicked')}>😀 😎 👍 💯</button>
  ))
  
  .add('tag', () => (
    <ButtonTemplate raised={false} class="tag">Tag Button</ButtonTemplate>
  ))

  .add('normal', () => (
    <MuiThemeProvider theme={Theme}>
       	<Grid container>
			<Grid item md={3}>
				<ButtonTemplate raised={true} class="normal" color="primary">Normal Button</ButtonTemplate>
			</Grid>
			<Grid item md={3}>
				<ButtonTemplate raised={true} class="normal" color="accent">Accent Button</ButtonTemplate>
			</Grid>
			<Grid item md={3}>
				<ButtonTemplate raised={true} class="normal" color="primary" disabled={true}>Disabled Button</ButtonTemplate>
			</Grid>
		</Grid>
    </MuiThemeProvider>
  ));
 