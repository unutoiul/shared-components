import blue from 'material-ui/colors/blue';
import green from 'material-ui/colors/green';
import red from 'material-ui/colors/red';
// import blue from 'themes/colors/blue';

import { createMuiTheme } from 'material-ui/styles';

const Primary = createMuiTheme({
    palette: {
      primary: blue, // Purple and green play nicely together.
      secondary: {
        A400: '#00e677',
        ...green
      },
      error: red,
    },
})

export default Primary;