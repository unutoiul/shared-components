import React, { Component } from 'react'
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import PropTypes from 'prop-types';

const styles = (theme) => {
    console.log(theme);
    return(
        {
            tag: {
                fontSize: '0.75rem',
                fontWeight: 600,
                textTransform: 'capitalize',
                borderRadius: '100px',
                boxShadow: theme.shadows[4],
                maxHeight: '30px',
            },
            normal: {
                borderRadius: '2px',
                fontSize: '0.75rem',
                boxShadow: theme.shadows[4],
                maxHeight: '40px',
            }
        }
    )
};

class ButtonTemplate extends Component {
    static propTypes = {
        color: PropTypes.string,
        classes: PropTypes.object.isRequired
    };

    render() {
        const {classes} = this.props;

        return(
            <Button raised={this.props.raised} disabled={this.props.disabled} color={this.props.color} className={classes[this.props.class]} >
                {this.props.children} 
            </Button>
        )
    }
}
  
export default withStyles(styles)(ButtonTemplate);